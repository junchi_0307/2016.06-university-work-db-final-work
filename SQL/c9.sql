-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- 主機: 127.0.0.1
-- 建立日期: 2018 年 07 月 25 日 15:27
-- 伺服器版本: 5.5.49-0ubuntu0.14.04.1
-- PHP 版本: 5.5.9-1ubuntu4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 資料庫: `c9`
--

-- --------------------------------------------------------

--
-- 資料表結構 `供應商`
--

CREATE TABLE IF NOT EXISTS `供應商` (
  `供應商代號` varchar(10) NOT NULL,
  `供應商名稱` varchar(15) NOT NULL,
  `電話` varchar(15) NOT NULL,
  `地址` varchar(30) NOT NULL,
  PRIMARY KEY (`供應商代號`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `供應商`
--

INSERT INTO `供應商` (`供應商代號`, `供應商名稱`, `電話`, `地址`) VALUES
('S001', '高應', '073814526', '高雄市'),
('S002', 'HELLO', '031287465', '哈哈呵呵路'),
('S003', '無吳', '059684521', '快樂村');

-- --------------------------------------------------------

--
-- 資料表結構 `倉庫`
--

CREATE TABLE IF NOT EXISTS `倉庫` (
  `倉庫ID` varchar(10) NOT NULL,
  `倉庫名稱` varchar(5) NOT NULL,
  `倉庫位置` varchar(30) NOT NULL,
  PRIMARY KEY (`倉庫ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `倉庫`
--

INSERT INTO `倉庫` (`倉庫ID`, `倉庫名稱`, `倉庫位置`) VALUES
('10', '紅倉庫', '第二層'),
('66', '我家', '樓下'),
('99', '我家吧', '隔壁右邊');

-- --------------------------------------------------------

--
-- 資料表結構 `員工`
--

CREATE TABLE IF NOT EXISTS `員工` (
  `員工ID` varchar(10) NOT NULL,
  `員工姓名` varchar(20) NOT NULL,
  `員工電話` varchar(10) NOT NULL,
  `員工地址` varchar(30) NOT NULL,
  `account` varchar(20) NOT NULL,
  `password` varchar(48) NOT NULL,
  `grade` varchar(2) NOT NULL,
  PRIMARY KEY (`員工ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `員工`
--

INSERT INTO `員工` (`員工ID`, `員工姓名`, `員工電話`, `員工地址`, `account`, `password`, `grade`) VALUES
('a', 'a', 'a', 'a', 'a', 'e2a0e5e38b778421cceafbfe9a37068b032093fd36be1635', '2'),
('b', 'b', 'b', 'b', 'b', '9bee8d76cd15cfbb7b7fb80567c3048bb04d08fa4e16406e', '2'),
('c', 'c', 'c', 'c', 'c', '4a6ca494aa1360e10d1e3dea9a5182cba57e0ca74d2edb35', '2'),
('d', 'd', 'd', 'd', 'd', 'cdcb48dcf180eca8939e4dea400e7c0d70fc42d9b0396755', '2'),
('root', '管理者', '管理者電話', '管理者地址', 'root', '62d5975a89044e9d4c07d9287940d6b978740ba3d6a43a51', '1');

-- --------------------------------------------------------

--
-- 資料表結構 `商品`
--

CREATE TABLE IF NOT EXISTS `商品` (
  `商品型號` int(10) NOT NULL,
  `商品名稱` varchar(20) NOT NULL,
  `單價` int(3) NOT NULL,
  `圖片位置` varchar(100) NOT NULL,
  `單位` varchar(1) NOT NULL,
  `材質` varchar(10) NOT NULL,
  `尺寸` varchar(20) NOT NULL,
  `商品種類商品總類ID` varchar(10) NOT NULL,
  `倉庫倉庫ID` varchar(10) NOT NULL,
  `供應商供應商代號` varchar(10) NOT NULL,
  PRIMARY KEY (`商品型號`),
  KEY `FK商品600417` (`商品種類商品總類ID`),
  KEY `FK商品568340` (`倉庫倉庫ID`),
  KEY `FK商品600000` (`供應商供應商代號`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `商品`
--

INSERT INTO `商品` (`商品型號`, `商品名稱`, `單價`, `圖片位置`, `單位`, `材質`, `尺寸`, `商品種類商品總類ID`, `倉庫倉庫ID`, `供應商供應商代號`) VALUES
(1001, '雙口爐', 750, 'Tulips.jpg', '台', 'N', 'N', 'A01', '10', 'S001'),
(1002, '一般瓦斯爐', 500, 'Koala.jpg', '台', 'N', 'N', 'A01', '66', 'S001');

-- --------------------------------------------------------

--
-- 資料表結構 `商品種類`
--

CREATE TABLE IF NOT EXISTS `商品種類` (
  `商品種類ID` varchar(10) NOT NULL,
  `商品總類` varchar(8) NOT NULL,
  PRIMARY KEY (`商品種類ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `商品種類`
--

INSERT INTO `商品種類` (`商品種類ID`, `商品總類`) VALUES
('A01', '瓦斯爐');

-- --------------------------------------------------------

--
-- 資料表結構 `盤點紀錄`
--

CREATE TABLE IF NOT EXISTS `盤點紀錄` (
  `盤點紀錄ID` int(5) NOT NULL AUTO_INCREMENT,
  `數量` int(4) NOT NULL,
  `盤點日期` date NOT NULL,
  `員工員工ID` varchar(10) NOT NULL,
  `商品商品型號` int(10) NOT NULL,
  PRIMARY KEY (`盤點紀錄ID`),
  KEY `FK盤點紀錄695682` (`商品商品型號`),
  KEY `盤點` (`員工員工ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 資料表結構 `訂製商品明細`
--

CREATE TABLE IF NOT EXISTS `訂製商品明細` (
  `訂製商品明細ID` int(5) NOT NULL AUTO_INCREMENT,
  `訂製商品名稱` varchar(20) NOT NULL,
  `訂製商品型號` varchar(10) NOT NULL,
  `訂製商品數量` int(4) NOT NULL,
  `訂製商品單位` varchar(1) NOT NULL,
  `訂製商品金額` int(8) NOT NULL,
  `訂製工程報價單訂製工程報價單編號` int(5) NOT NULL,
  `供應商供應商代號` varchar(10) NOT NULL,
  PRIMARY KEY (`訂製商品明細ID`),
  KEY `FK訂製商品明細608486` (`供應商供應商代號`),
  KEY `FK訂製商品明細387651` (`訂製工程報價單訂製工程報價單編號`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 資料表結構 `訂製工程報價單`
--

CREATE TABLE IF NOT EXISTS `訂製工程報價單` (
  `訂製工程報價單編號` int(5) NOT NULL AUTO_INCREMENT,
  `報價日期` date NOT NULL,
  `簽約日期` date NOT NULL,
  `交貨日期` date NOT NULL,
  `顧客顧客ID` varchar(10) NOT NULL,
  `員工員工ID` varchar(10) NOT NULL,
  `供應商供應商代號` varchar(10) NOT NULL,
  `訂製進度` varchar(10) NOT NULL,
  `備註` varchar(300) NOT NULL,
  PRIMARY KEY (`訂製工程報價單編號`),
  KEY `FK訂製工程報價單909547` (`員工員工ID`),
  KEY `FK訂製工程報價單650530` (`供應商供應商代號`),
  KEY `訂製` (`顧客顧客ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 資料表結構 `退換貨單`
--

CREATE TABLE IF NOT EXISTS `退換貨單` (
  `退換貨編號` int(5) NOT NULL AUTO_INCREMENT,
  `退換貨日期` date NOT NULL,
  `員工員工ID` varchar(10) NOT NULL,
  `銷貨單銷售單號` int(5) NOT NULL,
  `退換貨原因` varchar(500) NOT NULL,
  PRIMARY KEY (`退換貨編號`),
  KEY `FK退換貨單74840` (`銷貨單銷售單號`),
  KEY `退換貨` (`員工員工ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 資料表結構 `進貨單`
--

CREATE TABLE IF NOT EXISTS `進貨單` (
  `進貨單號` int(5) NOT NULL AUTO_INCREMENT,
  `通知日期` date NOT NULL,
  `進貨日期` date NOT NULL,
  `員工員工ID` varchar(10) NOT NULL,
  `供應商供應商代號` varchar(10) NOT NULL,
  `進貨進度` varchar(10) NOT NULL,
  `備註` varchar(300) NOT NULL,
  PRIMARY KEY (`進貨單號`),
  KEY `FK進貨單836915` (`供應商供應商代號`),
  KEY `進貨` (`員工員工ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 資料表的匯出資料 `進貨單`
--

INSERT INTO `進貨單` (`進貨單號`, `通知日期`, `進貨日期`, `員工員工ID`, `供應商供應商代號`, `進貨進度`, `備註`) VALUES
(2, '2016-06-22', '2016-06-23', 'root', 'S001', '未收貨', ''),
(3, '2016-06-14', '2016-06-29', 'root', 'S001', '未收貨', '');

-- --------------------------------------------------------

--
-- 資料表結構 `進貨紀錄`
--

CREATE TABLE IF NOT EXISTS `進貨紀錄` (
  `進貨紀錄ID` int(5) NOT NULL AUTO_INCREMENT,
  `進貨單進貨單號` int(5) NOT NULL,
  `商品商品型號` int(10) NOT NULL,
  `進貨單價` int(8) NOT NULL,
  `進貨數量` int(4) NOT NULL,
  PRIMARY KEY (`進貨紀錄ID`),
  KEY `FK進貨紀錄336322` (`進貨單進貨單號`),
  KEY `FK進貨紀錄189578` (`商品商品型號`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- 資料表的匯出資料 `進貨紀錄`
--

INSERT INTO `進貨紀錄` (`進貨紀錄ID`, `進貨單進貨單號`, `商品商品型號`, `進貨單價`, `進貨數量`) VALUES
(4, 3, 1002, 500, 5),
(5, 3, 1002, 500, 9),
(6, 2, 1002, 500, 1),
(7, 2, 1002, 500, 1);

-- --------------------------------------------------------

--
-- 資料表結構 `進貨退貨單`
--

CREATE TABLE IF NOT EXISTS `進貨退貨單` (
  `進貨退貨單編號` int(5) NOT NULL AUTO_INCREMENT,
  `退貨日期` date NOT NULL,
  `進貨單進貨單號` int(5) NOT NULL,
  `員工員工ID` varchar(10) NOT NULL,
  `退貨原因` varchar(500) NOT NULL,
  PRIMARY KEY (`進貨退貨單編號`),
  KEY `FK進貨退貨單392369` (`進貨單進貨單號`),
  KEY `FK進貨退貨單614086` (`員工員工ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 資料表結構 `銷貨單`
--

CREATE TABLE IF NOT EXISTS `銷貨單` (
  `銷售單號` int(5) NOT NULL AUTO_INCREMENT,
  `銷貨日期` date NOT NULL,
  `送貨日期` date NOT NULL,
  `員工員工ID` varchar(10) NOT NULL,
  `顧客顧客ID` varchar(10) NOT NULL,
  `銷貨進度` varchar(10) NOT NULL,
  `備註` varchar(300) NOT NULL,
  PRIMARY KEY (`銷售單號`),
  KEY `FK銷貨單872303` (`顧客顧客ID`),
  KEY `銷貨` (`員工員工ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 資料表結構 `銷貨紀錄`
--

CREATE TABLE IF NOT EXISTS `銷貨紀錄` (
  `銷貨紀錄ID` int(5) NOT NULL AUTO_INCREMENT,
  `銷貨數量` int(3) NOT NULL,
  `銷貨單銷售單號` int(5) NOT NULL,
  `商品商品型號` int(10) NOT NULL,
  PRIMARY KEY (`銷貨紀錄ID`),
  KEY `FK銷貨紀錄440965` (`銷貨單銷售單號`),
  KEY `FK銷貨紀錄974363` (`商品商品型號`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 資料表結構 `顧客`
--

CREATE TABLE IF NOT EXISTS `顧客` (
  `顧客ID` varchar(10) NOT NULL,
  `顧客姓名` varchar(20) NOT NULL,
  `顧客電話` varchar(15) NOT NULL,
  `顧客手機` varchar(15) NOT NULL,
  `顧客住址` varchar(30) NOT NULL,
  PRIMARY KEY (`顧客ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `顧客`
--

INSERT INTO `顧客` (`顧客ID`, `顧客姓名`, `顧客電話`, `顧客手機`, `顧客住址`) VALUES
('abc123', '王中明', '0492733516', '09123789456', '深水區132路'),
('ABC321', '愈早錢', '059684517', '0985462458', '台中市新北區花蓮鎮');

--
-- 已匯出資料表的限制(Constraint)
--

--
-- 資料表的 Constraints `商品`
--
ALTER TABLE `商品`
  ADD CONSTRAINT `FK商品568340` FOREIGN KEY (`倉庫倉庫ID`) REFERENCES `倉庫` (`倉庫ID`),
  ADD CONSTRAINT `FK商品600000` FOREIGN KEY (`供應商供應商代號`) REFERENCES `供應商` (`供應商代號`),
  ADD CONSTRAINT `FK商品600417` FOREIGN KEY (`商品種類商品總類ID`) REFERENCES `商品種類` (`商品種類ID`);

--
-- 資料表的 Constraints `盤點紀錄`
--
ALTER TABLE `盤點紀錄`
  ADD CONSTRAINT `盤點` FOREIGN KEY (`員工員工ID`) REFERENCES `員工` (`員工ID`),
  ADD CONSTRAINT `FK盤點紀錄695682` FOREIGN KEY (`商品商品型號`) REFERENCES `商品` (`商品型號`);

--
-- 資料表的 Constraints `訂製商品明細`
--
ALTER TABLE `訂製商品明細`
  ADD CONSTRAINT `FK訂製商品明細387651` FOREIGN KEY (`訂製工程報價單訂製工程報價單編號`) REFERENCES `訂製工程報價單` (`訂製工程報價單編號`),
  ADD CONSTRAINT `FK訂製商品明細608486` FOREIGN KEY (`供應商供應商代號`) REFERENCES `供應商` (`供應商代號`);

--
-- 資料表的 Constraints `訂製工程報價單`
--
ALTER TABLE `訂製工程報價單`
  ADD CONSTRAINT `訂製` FOREIGN KEY (`顧客顧客ID`) REFERENCES `顧客` (`顧客ID`),
  ADD CONSTRAINT `FK訂製工程報價單650530` FOREIGN KEY (`供應商供應商代號`) REFERENCES `供應商` (`供應商代號`),
  ADD CONSTRAINT `FK訂製工程報價單909547` FOREIGN KEY (`員工員工ID`) REFERENCES `員工` (`員工ID`);

--
-- 資料表的 Constraints `退換貨單`
--
ALTER TABLE `退換貨單`
  ADD CONSTRAINT `退換貨` FOREIGN KEY (`員工員工ID`) REFERENCES `員工` (`員工ID`),
  ADD CONSTRAINT `FK退換貨單74840` FOREIGN KEY (`銷貨單銷售單號`) REFERENCES `銷貨單` (`銷售單號`);

--
-- 資料表的 Constraints `進貨單`
--
ALTER TABLE `進貨單`
  ADD CONSTRAINT `進貨` FOREIGN KEY (`員工員工ID`) REFERENCES `員工` (`員工ID`),
  ADD CONSTRAINT `FK進貨單836915` FOREIGN KEY (`供應商供應商代號`) REFERENCES `供應商` (`供應商代號`);

--
-- 資料表的 Constraints `進貨紀錄`
--
ALTER TABLE `進貨紀錄`
  ADD CONSTRAINT `FK進貨紀錄189578` FOREIGN KEY (`商品商品型號`) REFERENCES `商品` (`商品型號`),
  ADD CONSTRAINT `FK進貨紀錄336322` FOREIGN KEY (`進貨單進貨單號`) REFERENCES `進貨單` (`進貨單號`);

--
-- 資料表的 Constraints `進貨退貨單`
--
ALTER TABLE `進貨退貨單`
  ADD CONSTRAINT `FK進貨退貨單392369` FOREIGN KEY (`進貨單進貨單號`) REFERENCES `進貨單` (`進貨單號`),
  ADD CONSTRAINT `FK進貨退貨單614086` FOREIGN KEY (`員工員工ID`) REFERENCES `員工` (`員工ID`);

--
-- 資料表的 Constraints `銷貨單`
--
ALTER TABLE `銷貨單`
  ADD CONSTRAINT `銷貨` FOREIGN KEY (`員工員工ID`) REFERENCES `員工` (`員工ID`),
  ADD CONSTRAINT `FK銷貨單872303` FOREIGN KEY (`顧客顧客ID`) REFERENCES `顧客` (`顧客ID`);

--
-- 資料表的 Constraints `銷貨紀錄`
--
ALTER TABLE `銷貨紀錄`
  ADD CONSTRAINT `FK銷貨紀錄440965` FOREIGN KEY (`銷貨單銷售單號`) REFERENCES `銷貨單` (`銷售單號`),
  ADD CONSTRAINT `FK銷貨紀錄974363` FOREIGN KEY (`商品商品型號`) REFERENCES `商品` (`商品型號`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
