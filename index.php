<html>
    <head>
        <title>登入</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">;
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <style>
            body {                
                background: url("index_back.jpg") center center fixed no-repeat;
                margin:0px; padding:0px;
                background-size: cover;
                background-attachment: fixed;
            }
        </style>
        <style type="text/css">
            .pos1{
                position: relative;
                top: 25%;
                left: 25%;
            }
        </style>
    </head>
    <body>
        <form align="center" class="pos1" method="post" action="connect.php" style="width:50%;">
            
            <div style="padding:10px; background-color:#CCCCCC; opacity:0.8; height:50%; font-size:4em">
                <div class="from-signin-heading" style="font-size:1.25em">使用者登入</div>
                <input type="text" name="at" style="height:80px; font-size:40px" class="form-control" placeholder="帳號">
                <input type="password" name="pw" style="height:80px; font-size:40px" class="form-control" placeholder="密碼"> <br>
                <input type="submit" class="btn btn-primary" value="登入" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="reset" class="btn btn-primary" value="重設"/>
            </div>
        </form>
    </body>
</html>
