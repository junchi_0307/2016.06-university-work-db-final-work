<html>
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <?php
        require 'mysql_connect.php';
        $hidden=$_POST['hidden'];
            if($hidden=="SD_new"){
                $total_chack=true;
                $SD_list=$_POST['SD_list'];
                $SD_list_Q=$_POST['SD_list_Q'];
                $customer_id=$_POST['customer_id'];
                $employee_id=$_POST['employee_id'];
                $delivery_date=$_POST['delivery_date'];
                $SD_date=$_POST['SD_date'];
                $remark=$_POST['remark'];
                
                    $SQL="INSERT INTO 銷貨單 (`銷售單號`,`送貨日期`,`銷貨日期`,`員工員工ID`,`顧客顧客ID`,`銷貨進度`,`備註`) VALUES ('NULL',:delivery_date,:SD_date,:employee_id,:customer_id,:SD_speed,:remark)";
                    $input=array(':delivery_date'=>$delivery_date,':SD_date'=>$SD_date,':employee_id'=>$employee_id,':customer_id'=>$customer_id,':SD_speed'=>'未出貨',':remark'=>$remark);
                    $db = Database::initDB();
                    $temp=$db->prepare($SQL);
                    $check=$temp->execute($input);
                    $SD_id=$db->lastInsertId();
                    $db=NULL;
                    if(!($check>0)){
                        $total_chack=false;
                    }
                    if($total_chack){
                        for($i=0;$i<count($SD_list);$i++){
                            $SQL="INSERT INTO 銷貨紀錄 (`銷貨紀錄ID`,`銷貨單銷售單號`,`商品商品型號`,`銷貨數量`) VALUES ('NULL',:SD_id,:SD_list,:SD_list_Q)";
                            $input=array(':SD_id'=>$SD_id,':SD_list'=>$SD_list[$i],':SD_list_Q'=>$SD_list_Q[$i]);
                            $db = Database::initDB();
                            $temp=$db-> prepare($SQL);
                            $check=$temp->execute($input);
                            $db=NULL;
                            if(!($check>0)){
                                $total_chack=false;
                            }
                        }
                    }
                    
                if($total_chack){
                    echo '新增成功';
                }else{
                    echo '新增失敗';
                    $SD_id="";
                }//新增區塊
            }else if($hidden=="SD_revise"){
                $SD_id=$_POST['SD_id'];
                $total_chack=true;
                $SD_list=$_POST['SD_list'];
                $SD_list_Q=$_POST['SD_list_Q'];
                $customer_name=$_POST['customer_name'];
                $employee_name=$_POST['employee_name'];
                $delivery_date=$_POST['delivery_date'];
                $SD_date=$_POST['SD_date'];
                $SD_speed=$_POST['SD_speed'];
                $remark=$_POST['remark'];
                
                {$customer_name_check=false;
                $db = Database::initDB();
                $temp = $db -> query("select 顧客姓名 from 顧客");
                $db=NULL;
                foreach($temp->fetchall() as $row){
                    if($row[0]==$customer_name){
                        $customer_name_check=true;
                    }
                }
                $employee_name_check=false;
                $db = Database::initDB();
                $temp = $db -> query("select 員工姓名 from 員工");
                $db=NULL;
                foreach($temp->fetchall() as $row){
                    if($row[0]==$employee_name){
                        $employee_name_check=true;
                    }
                }
                
                $SD_list_check_total=true;
                for($i=0;$i<count($SD_list);$i++){
                    $SD_list_check=false;
                    $db = Database::initDB();
                    $temp = $db -> query("select 商品名稱 from 商品");
                    $db=NULL;
                    foreach($temp->fetchall() as $row){
                        if($row[0]==$SD_list[$i]){
                            $SD_list_check=true;
                        }
                    }
                    if(!$SD_list_check){
                        $SD_list_check_total=false;
                    }
                }
                    
                }//測試是否有輸入的商品名稱,顧客姓名,員工姓名
                
                if($SD_list_check_total && $employee_name_check && $customer_name_check){
                    $SQL="DELETE FROM 銷貨紀錄 WHERE 銷貨單銷售單號 = :SD_id";
                    $db = Database::initDB();
                    $temp=$db-> prepare($SQL);
                    $check=$temp->execute(array(':SD_id'=>$SD_id));
                    $db=NULL;
                    /*if(!($check>0)){
                        $total_chack=false;
                    }*/
                    
                    $SQL="UPDATE 銷貨單 SET `送貨日期`=:delivery_date,`銷貨日期`=:SD_date,`員工員工ID`=(select 員工ID from 員工 where 員工姓名=:employee_name), `顧客顧客ID`=(select 顧客ID from 顧客 where 顧客姓名=:customer_name),`銷貨進度`=:SD_speed,`備註`=:remark where `銷售單號`=:SD_id";
                    $input=array(':SD_id'=>$SD_id,':delivery_date'=>$delivery_date,':SD_date'=>$SD_date,':employee_name'=>$employee_name,':customer_name'=>$customer_name,':SD_speed'=>'未出貨',':remark'=>$remark);
                    $db = Database::initDB();
                    $temp=$db-> prepare($SQL);
                    $check=$temp->execute($input);
                    $db=NULL;
                    if(!($check>0)){
                        $total_chack=false;
                    }
                    if($total_chack){
                        for($i=0;$i<count($SD_list);$i++){
                            $SQL="INSERT INTO 銷貨紀錄 (`銷貨紀錄ID`,`銷貨單銷售單號`,`商品商品型號`,`銷貨數量`) VALUES ('NULL',:SD_id,(select 商品型號 from 商品 where 商品名稱 = :SD_list),:SD_list_Q)";
                            $input=array(':SD_id'=>$SD_id,':SD_list'=>$SD_list[$i],':SD_list_Q'=>$SD_list_Q[$i]);
                            $db = Database::initDB();
                            $temp=$db-> prepare($SQL);
                            $check=$temp->execute($input);
                            $db=NULL;
                            if(!($check>0)){
                                $total_chack=false;
                            }
                        }
                    }
                }else{
                    $total_chack=false;
                }//update 銷貨單,銷貨紀錄  若任何一條輸入錯誤則$total_chack會變成false
                
                if($total_chack){
                    echo '修改成功';
                }else{
                    echo '修改失敗';
                }//修改區塊
            }else if($hidden=="SD_del"){
                $SD_id=$_POST['SD_id'];
                $total_chack=true;
                $SQL="DELETE FROM 銷貨紀錄 WHERE 銷貨單銷售單號 = :SD_id";
                $db = Database::initDB();
                $temp=$db-> prepare($SQL);
                $check=$temp->execute(array(':SD_id'=>$SD_id));
                $db=NULL;
                if(!($check>0)){
                    $total_chack=false;
                }
                
                $SQL="DELETE FROM 銷貨單 WHERE 銷售單號 = :SD_id";
                $db = Database::initDB();
                $temp=$db-> prepare($SQL);
                $check=$temp->execute(array(':SD_id'=>$SD_id));
                $db=NULL;
                if(!($check>0)){
                    $total_chack=false;
                }
                
                if($total_chack){
                    echo '刪除成功';
                    $SD_id="";
                }else{
                    echo '刪除失敗';
                }
            
            //刪除區塊
            }else if($hidden=="SD_receipt"){
                $SD_id=$_POST['SD_id'];
                $SQL="UPDATE 銷貨單 SET `銷貨進度` =  '已出貨' WHERE  `銷售單號` =  :SD_id;";
                $input=array(':SD_id'=>$SD_id);
                $db = Database::initDB();
                $temp=$db->prepare($SQL);
                $check=$temp->execute($input);
                $db=NULL;
                
                if($check>0){
                    echo '出貨完成';
                }
                
            
            }else if($hidden=="SD_return"){
                $SD_id=$_POST['SD_id'];
                $SD_return_id=$_POST['SD_return_id'];
                $SD_return_employee_name=$_POST['SD_return_employee_name'];
                $SD_return_date=$_POST['SD_return_date'];
                $SD_return_remark=$_POST['SD_return_remark'];
                
                $SQL="INSERT INTO 退換貨單 (`退換貨編號`,`退換貨日期`,`銷貨單銷售單號`,`員工員工ID`,`退換貨原因`) VALUES (:SD_return_id,:SD_return_date,:SD_id,(select 員工ID from 員工 where 員工姓名=:SD_return_employee_name),:SD_return_remark )";
                $input=array(':SD_return_id'=>$SD_return_id,':SD_return_date'=>$SD_return_date,':SD_id'=>$SD_id,':SD_return_employee_name'=>$SD_return_employee_name,':SD_return_remark'=>$SD_return_remark);
                $db= Database::initDB();
                $temp=$db-> prepare($SQL);
                $check=$temp->execute($input);
                $db=NULL;
                
                if($check>0){
                    $SQL="UPDATE 銷貨單 SET `銷貨進度` =  '已退貨' WHERE  `銷售單號` =  :SD_id;";
                    $input=array(':SD_id'=>$SD_id);
                    $db = Database::initDB();
                    $temp=$db->prepare($SQL);
                    $check=$temp->execute($input);
                    $db=NULL;
                    if($check>0){
                        echo "退貨成功";
                    }else{
                    echo "退貨失敗";
                    }
                }else{
                    echo "退貨失敗";
                }
            
            }else if($hidden=="SD_end"){
                    $SD_id=$_POST['SD_id'];
                    $SQL="UPDATE 銷貨單 SET `銷貨進度` =  '交易完成' WHERE  `銷售單號` =  :SD_id;";
                    $input=array(':SD_id'=>$SD_id);
                    $db = Database::initDB();
                    $temp=$db->prepare($SQL);
                    $check=$temp->execute($input);
                    $db=NULL;
                    if($check>0){
                        echo "交易完成";
                    }else{
                    echo "交易尚未完成";
                    }
            }
        ?>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;跳轉中請稍後~~~~
        <form action="SD_search.php" method="POST" id="form_id">
            <input type="hidden" name="SD_id" value=<?php echo $SD_id; ?>>
        </form>
        <script type="text/javascript">
            setTimeout("form_id.submit();",1000);
        </script>
    </body>
</html>
