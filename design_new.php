<html>
    <head>
        <meta charset="UTF-8">
        <script type="text/javascript">
            var text_total = 1;
            function add_text() {//增加一行
                var html = '<table><tr><td>訂製商品名稱</td><td>訂製商品型號</td><td>訂製商品數量</td><td>訂製商品單位</td><td>訂製商品金額</td><td>供應商供應商代號</td></tr>';
                for (var i = 0; i < text_total; i++) {
                    var text_temp = document.getElementById('design_name[' + i + ']').value;
                    var text_temp_type = document.getElementById('design_type[' + i + ']').value;
                    var text_temp_count = document.getElementById('design_count[' + i + ']').value;
                    var text_countname = document.getElementById('design_countname[' + i + ']').value;
                    var text_dollar = document.getElementById('design_dollar[' + i + ']').value;
                    var text_supplier_name = document.getElementById('design_supplier_name[' + i + ']').value;
                    html += '<tr><td><input type="text" name="design_name[' + i + ']" value="' + text_temp + '" id="design_name[' + i + ']"></td>';
                    html += '<td><input type="text" name="design_type[' + i + ']" value="' + text_temp_type + '" id="design_type[' + i + ']"></td>';
                    html += '<td><input type="text" name="design_count[' + i + ']" value="' + text_temp_count + '" id="design_count[' + i + ']"></td>';
                    html += '<td><input type="text" name="design_countname[' + i + ']" value="' + text_countname + '" id="design_countname[' + i + ']"></td>';
                    html += '<td><input type="text" name="design_dollar[' + i + ']" value="' + text_dollar + '" id="design_dollar[' + i + ']"></td>';
                    html += '<td><input type="text" name="design_supplier_name[' + i + ']" value="' + text_supplier_name + '" id="design_supplier_name[' + i + ']"></td></tr>';
                }
                html += '<tr><td><input type="text" name="design_name[' + text_total + ']" value="" id="design_name[' + text_total + ']"></td>';
                html += '<td><input type="text" name="design_type[' + text_total + ']" value="" id="design_type[' + text_total + ']"></td>';
                html += '<td><input type="text" name="design_count[' + text_total + ']" value="" id="design_count[' + text_total + ']"></td>';
                html += '<td><input type="text" name="design_countname[' + text_total + ']" value="" id="design_countname[' + text_total + ']"></td>';
                html += '<td><input type="text" name="design_dollar[' + text_total + ']" value="" id="design_dollar[' + text_total + ']"></td>';
                html += '<td><input type="text" name="design_supplier_name[' + text_total + ']" value="" id="design_supplier_name[' + text_total + ']"></td></tr></table>';
                text_total++;
                document.getElementById('text_zone').innerHTML = html;
            }
            function clear_text() {//清除
                text_total--;
                var html = '<table><tr><td>訂製商品名稱</td><td>訂製商品型號</td><td>訂製商品數量</td><td>訂製商品單位</td><td>訂製商品金額</td><td>供應商供應商代號</td></tr>';
                for (var i = 0; i <= text_total; i++) {
                    html += '<tr><td><input type="text" name="design_name[' + i + ']" value="" id="design_name[' + i + ']"></td>';
                    html += '<td><input type="text" name="design_type[' + i + ']" value="" id="design_type[' + i + ']"></td>';
                    html += '<td><input type="text" name="design_count[' + i + ']" value="" id="design_count[' + i + ']"></td>';
                    html += '<td><input type="text" name="design_countname[' + i + ']" value="" id="design_countname[' + i + ']"></td>';
                    html += '<td><input type="text" name="design_dollar[' + i + ']" value="" id="design_dollar[' + i + ']"></td>';
                    html += '<td><input type="text" name="design_supplier_name[' + i + ']" value="" id="design_supplier_name[' + i + ']"></td></tr>';
                }
                document.getElementById('text_zone').innerHTML = html;
                text_total++;
            }
            function del_text() {//減少一行
                if (text_total > 1) {
                    text_total--;
                    var html = '<table><tr><td>訂製商品名稱</td><td>訂製商品型號</td><td>訂製商品數量</td><td>訂製商品單位</td><td>訂製商品金額</td><td>供應商供應商代號</td></tr>';
                for (var i = 0; i < text_total; i++) {
                    var text_temp = document.getElementById('design_name[' + i + ']').value;
                    var text_temp_type = document.getElementById('design_type[' + i + ']').value;
                    var text_temp_count = document.getElementById('design_count[' + i + ']').value;
                    var text_countname = document.getElementById('design_countname[' + i + ']').value;
                    var text_dollar = document.getElementById('design_dollar[' + i + ']').value;
                    var text_supplier_name = document.getElementById('design_supplier_name[' + i + ']').value;
                    html += '<tr><td><input type="text" name="design_name[' + i + ']" value="' + text_temp + '" id="design_name[' + i + ']"></td>';
                    html += '<td><input type="text" name="design_type[' + i + ']" value="' + text_temp_type + '" id="design_type[' + i + ']"></td>';
                    html += '<td><input type="text" name="design_count[' + i + ']" value="' + text_temp_count + '" id="design_count[' + i + ']"></td>';
                    html += '<td><input type="text" name="design_countname[' + i + ']" value="' + text_countname + '" id="design_countname[' + i + ']"></td>';
                    html += '<td><input type="text" name="design_dollar[' + i + ']" value="' + text_dollar + '" id="design_dollar[' + i + ']"></td>';
                    html += '<td><input type="text" name="design_supplier_name[' + i + ']" value="' + text_supplier_name + '" id="design_supplier_name[' + i + ']"></td></tr>';
                }
                    document.getElementById('text_zone').innerHTML = html;
                }
            } 
            function acc() {//確認有無所有欄位皆有值
                for (var i = 0; i < text_total; i++) {
                    if (document.getElementById('design_name[' + i + ']').value == "") {
                        document.getElementById('display').innerHTML = '<font style="color:red;"  size="4"><<請勿留下空白欄位>></font>';
                        return false;
                    }
                    if (document.getElementById('design_type[' + i + ']').value == "") {
                        document.getElementById('display').innerHTML = '<font style="color:red;"  size="4"><<請勿留下空白欄位>></font>';
                        return false;
                    }
                    if (document.getElementById('design_count[' + i + ']').value == "") {
                        document.getElementById('display').innerHTML = '<font style="color:red;"  size="4"><<請勿留下空白欄位>></font>';
                        return false;
                    }
                    if (document.getElementById('design_countname[' + i + ']').value == "") {
                        document.getElementById('display').innerHTML = '<font style="color:red;"  size="4"><<請勿留下空白欄位>></font>';
                        return false;
                    }
                    if (document.getElementById('design_dollar[' + i + ']').value == "") {
                        document.getElementById('display').innerHTML = '<font style="color:red;"  size="4"><<請勿留下空白欄位>></font>';
                        return false;
                    }
                    if (document.getElementById('design_supplier_name[' + i + ']').value == "") {
                        document.getElementById('display').innerHTML = '<font style="color:red;"  size="4"><<請勿留下空白欄位>></font>';
                        return false;
                    }
                }
                if ((document.getElementById("design_id").value == "") || (document.getElementById("price_date").value == "") || (document.getElementById("deal_date").value == "") || (document.getElementById("finish_date").value == "") || (document.getElementById("customer_id").value == "") || (document.getElementById("employee_id").value == "")) {
                    document.getElementById('display').innerHTML = '<font style="color:red;"  size="4"><<請勿留下空白欄位>></font>';
                    //return false;
                }
                return true;
            }
        </script>
    </head>

    <body>
        
        <div id="display"><!--若有空白欄位，則這邊會更新顯示-->
            
        </div>
        <form action="design_temp.php" method="POST" onsubmit="return acc();">
            <table>
                <tr>
                    <td>訂製工程報價單編號：</td><td><input type="text" name="design_id" value="" id="design_id"></td>
                    <td>報價日期：</td><td><input type="date" name="price_date" value="" id="price_date"></td>
                </tr>
            <tr>
                <td>簽約日期：</td><td><input type="date" name=" deal_date" value="" id="deal_date"></td>
                <td>交貨日期：</td><td><input type="date" name="finish_date" value="" id="finish_date"></td>
            </tr>
            <tr>
                <td>顧客姓名：</td><td><input type="text" name="customer_name" value="" id="customer_name"></td>
                <td>員工姓名：</td><td><input type="text" name="employee_name" value="" id="employee_name"></td>
            </tr>
            <tr>
                <td><input type="button" value="新增一行" onclick="add_text()"></td><td><input type="button" value="刪除一行" onclick="del_text()"></td>
            </tr>
            </table>
            <div id="text_zone">
                <table>
                    <tr>
                        <td>訂製商品名稱</td><td>訂製商品型號</td><td>訂製商品數量</td><td>訂製商品單位</td><td>訂製商品金額</td><td>供應商名稱</td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" name="design_name[0]" id="design_name[0]">
                        </td>
                        <td>
                            <input type="text" name="design_type[0]" id="design_type[0]">
                        </td>
                        <td>
                            <input type="text" name="design_count[0]" id="design_count[0]">
                        </td>
                        <td>
                            <input type="text" name="design_countname[0]" id="design_countname[0]">
                        </td>
                        <td>
                            <input type="text" name="design_dollar[0]" id="design_dollar[0]">
                        </td>
                        <td>
                            <input type="text" name="design_supplier_name[0]" id="design_supplier_name[0]">
                        </td>
                    </tr>
                </table>
            </div>
            <textarea name="remark" row="10" cols="20" warp="off"></textarea></br>
            <input type="submit" value="送出" onclick="acc()">
            <input type="button" value="清除" onclick="clear_text()">
            <input type="hidden" name="hidden" value="design_new">
        </form>
    </body>
</html>
