<html>
    <head>
        <meta charset="UTF-8">
        <script type="text/javascript">
            var text_total = 1;
            function add_text() {//增加一行
                var html = '<table><tr><td>商品名稱</td><td>銷貨數量</td></tr>';
                for (var i = 0; i < text_total; i++) {
                    var text_INV_goods = document.getElementById('INV_goods[' + i + ']').value;
                    var text_INV_count = document.getElementById('INV_count[' + i + ']').value;
                    html += '<tr><td><input type="text" name="INV_goods[' + i + ']" value="' + text_INV_goods + '" id="INV_goods[' + i + ']"></td>';
                    html += '<td><input type="text" name="INV_count[' + i + ']" value="' + text_INV_count + '" id="INV_count[' + i + ']"></td></tr>';
                }
                html += '<tr><td><input type="text" name="INV_goods[' + text_total + ']" value="" id="INV_goods[' + text_total + ']"></td>';
                html += '<td><input type="text" name="INV_count[' + text_total + ']" value="" id="INV_count[' + text_total + ']"></td></tr></table>';
                text_total++;
                document.getElementById('text_zone').innerHTML = html;
            }
            function clear_text() {//清除
                text_total--;
                var html = '<table><tr><td>商品名稱</td><td>銷貨數量</td></tr>';
                for (var i = 0; i <= text_total; i++) {
                    html += '<tr><td><input type="text" name="INV_goods[' + i + ']" value="" id="INV_goods[' + i + ']"></td>';
                    html += '<td><input type="text" name="INV_count[' + i + ']" value="" id="INV_count[' + i + ']"></td></tr>';
                }
                document.getElementById('text_zone').innerHTML = html;
                text_total++;
            }
            function del_text() {//減少一行
                if (text_total > 1) {
                    text_total--;
                    var html = '<table><tr><td>商品名稱</td><td>銷貨數量</td></tr>';
                    for (var i = 0; i < text_total; i++) {
                        var text_INV_goods = document.getElementById('INV_goods[' + i + ']').value;
                        var text_INV_count = document.getElementById('INV_count[' + i + ']').value;
                        html += '<tr><td><input type="text" name="INV_goods[' + i + ']" value="' + text_INV_goods + '" id="INV_goods[' + i + ']"></td>';
                        html += '<td><input type="text" name="INV_count[' + i + ']" value="' + text_INV_count + '" id="INV_count[' + i + ']"></td></tr>';
                    }
                    document.getElementById('text_zone').innerHTML = html;
                }
            }
            function acc() {//確認有無所有欄位皆有值
                for (var i = 0; i < text_total; i++) {
                    if (document.getElementById('INV_goods[' + i + ']').value == "") {
                        document.getElementById('display').innerHTML = '<font style="color:red;"  size="4"><<請勿留下空白欄位>></font>';
                        return false;
                    }
                    if (document.getElementById('INV_count[' + i + ']').value == "") {
                        document.getElementById('display').innerHTML = '<font style="color:red;"  size="4"><<請勿留下空白欄位>></font>';
                        return false;
                    }
                }
                if ((document.getElementById("employee_name").value == "") || (document.getElementById("INV_date").value == "") || (document.getElementById("customer_name").value == "")) {
                    document.getElementById('display').innerHTML = '<font style="color:red;"  size="4"><<請勿留下空白欄位>></font>';
                    return false;
                }
                return true;
            }
        </script>
    </head>

    <body>
        
        <div id="display"><!--若有空白欄位，則這邊會更新顯示-->
            
        </div>
        <form action="INV_temp.php" method="POST" onsubmit="return acc();">
            <table>
                <tr>
                    <td> 盤點員工姓名：</td><td><input type="text" name="employee_name" value="" id="employee_name"></td>
                <td>盤點日期：</td><td><input type="date" name=" INV_date" value="" id="delivery_date"></td>
                </tr>
            <tr>
                <td><input type="button" value="新增一行" onclick="add_text()"></td><td><input type="button" value="刪除一行" onclick="del_text()"></td>
            </tr>
            </table>
            <div id="text_zone">
                <table>
                    <tr>
                        <td>
                            商品名稱
                        </td>
                        <td>
                            盤點數量
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" name="INV_goods[0]" id="INV_goods[0]">
                        </td>
                        <td>
                            <input type="text" name="INV_count[0]" id="INV_count[0]">
                        </td>
                    </tr>
                </table>
            </div>
            <input type="submit" value="送出" onclick="acc()">
            <input type="button" value="清除" onclick="clear_text()">
            <input type="hidden" name="hidden" value="INV_new">
        </form>
    </body>
</html>
