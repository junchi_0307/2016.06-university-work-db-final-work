<html>
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <?php
        require 'mysql_connect.php';
        $hidden=$_POST['hidden'];
        $design_id=$_POST['design_id'];
            if($hidden=="design_new"){
                $total_chack=true;
                $design_name=$_POST['design_name'];
                $design_count=$_POST['design_count'];
                $design_type=$_POST['design_type'];
                $design_countname=$_POST['design_countname'];
                $design_dollar=$_POST['design_dollar'];
                $design_supplier_name=$_POST['design_supplier_name'];
                $design_id=$_POST['design_id'];
                $price_date=$_POST['price_date'];
                $deal_date=$_POST['deal_date'];
                $finish_date=$_POST['finish_date'];
                $customer_name=$_POST['customer_name'];
                $employee_name=$_POST['employee_name'];
                $remark=$_POST['remark'];
                {$customer_name_check=false;
                $db = Database::initDB();
                $temp = $db -> query("select 顧客姓名 from 顧客");
                $db=NULL;
                foreach($temp->fetchall() as $row){
                    if($row[0]==$customer_name){
                        $customer_name_check=true;
                    }
                }
                $employee_name_check=false;
                $db = Database::initDB();
                $temp = $db -> query("select 員工姓名 from 員工");
                $db=NULL;
                foreach($temp->fetchall() as $row){
                    if($row[0]==$employee_name){
                        $employee_name_check=true;
                    }
                }
                }
                if($customer_name_check && $employee_name_check){
                    $SQL="INSERT INTO 訂製工程報價單 (`訂製工程報價單編號`,`報價日期`,`簽約日期`,`交貨日期`,`顧客顧客ID`,`員工員工ID`,`供應商供應商代號`,`訂製進度`,`備註`) VALUES (:design_id,:price_date,:deal_date,:finish_date,(select 顧客ID from 顧客 where 顧客姓名=:customer_name),(select 員工ID from 員工 where 員工姓名=:employee_name),'S001',:design_speed,:remark)";
                    $input=array(':design_id'=>$design_id,':price_date'=>$price_date,':deal_date'=>$deal_date,':customer_name'=>$customer_name,':finish_date'=>$finish_date,':employee_name'=>$employee_name,':design_speed'=>'已簽約',':remark'=>$remark);
                    $db = Database::initDB();
                    $temp=$db->prepare($SQL);
                    $check=$temp->execute($input);
                    $db=NULL;
                    if(!($check>0)){
                        $total_chack=false;
                    }
                    if($total_chack){
                        for($i=0;$i<count($design_name);$i++){
                            $SQL="INSERT INTO 訂製商品明細 (`訂製商品明細ID`,`訂製商品名稱`,`訂製商品型號`,`訂製商品數量`,`訂製商品單位`,`訂製商品金額`,`訂製工程報價單訂製工程報價單編號`,`供應商供應商代號`) VALUES ('NULL',:design_name,:design_type,:design_count,:design_countname,:design_dollar,:design_id,(select 供應商代號 from 供應商 where 供應商名稱=:design_supplier_name))";
                            $input=array(':design_name'=>$design_name[$i],':design_type'=>$design_type[$i],':design_count'=>$design_count[$i],':design_countname'=>$design_countname[$i],':design_dollar'=>$design_dollar[$i],':design_id'=>$design_id,':design_supplier_name'=>$design_supplier_name[$i]);
                            $db = Database::initDB();
                            $temp=$db-> prepare($SQL);
                            $check=$temp->execute($input);
                            $db=NULL;
                            if(!($check>0)){
                                $total_chack=false;
                            }
                        }
                    }
                }else{
                    $total_chack=false;
                }
                if($total_chack){
                    echo '新增成功';
                }else{
                    echo '新增失敗';
                }//新增區塊
            }else if($hidden=="design_revise"){
                $total_chack=true;
                $design_name=$_POST['design_name'];
                $design_count=$_POST['design_count'];
                $design_type=$_POST['design_type'];
                $design_countname=$_POST['design_countname'];
                $design_dollar=$_POST['design_dollar'];
                $design_supplier_name=$_POST['design_supplier_name'];
                $design_id=$_POST['design_id'];
                $price_date=$_POST['price_date'];
                $deal_date=$_POST['deal_date'];
                $finish_date=$_POST['finish_date'];
                $customer_name=$_POST['customer_name'];
                $employee_name=$_POST['employee_name'];
                $remark=$_POST['remark'];
                {$customer_name_check=false;
                $db = Database::initDB();
                $temp = $db -> query("select 顧客姓名 from 顧客");
                $db=NULL;
                foreach($temp->fetchall() as $row){
                    if($row[0]==$customer_name){
                        $customer_name_check=true;
                    }
                }
                $employee_name_check=false;
                $db = Database::initDB();
                $temp = $db -> query("select 員工姓名 from 員工");
                $db=NULL;
                foreach($temp->fetchall() as $row){
                    if($row[0]==$employee_name){
                        $employee_name_check=true;
                    }
                }
                }
                if($customer_name_check && $employee_name_check){
                    $SQL="DELETE FROM 訂製商品明細 WHERE 訂製工程報價單訂製工程報價單編號 = :design_id";
                    $db = Database::initDB();
                    $temp=$db-> prepare($SQL);
                    $check=$temp->execute(array(':design_id'=>$design_id));
                    $db=NULL;
                    /*if(!($check>0)){
                        $total_chack=false;
                    }*/
                    
                    $SQL="UPDATE 訂製工程報價單 SET `報價日期`=:price_date,`簽約日期`=:deal_date,`交貨日期`=:finish_date,`顧客顧客ID`=(select 顧客ID from 顧客 where 顧客姓名=:customer_name),`員工員工ID`=(select 員工ID from 員工 where 員工姓名=:employee_name),`備註`=:remark where `訂製工程報價單編號`=:design_id";
                    $input=array(':design_id'=>$design_id,':price_date'=>$price_date,':deal_date'=>$deal_date,':finish_date'=>$finish_date,':customer_name'=>$customer_name,':employee_name'=>$employee_name,':remark'=>$remark);
                    $db = Database::initDB();
                    $temp=$db-> prepare($SQL);
                    $check=$temp->execute($input);
                    $db=NULL;
                    if(!($check>0)){
                        $total_chack=false;
                    }
                    if($total_chack){
                        for($i=0;$i<count($design_name);$i++){
                            $SQL="INSERT INTO 訂製商品明細 (`訂製商品明細ID`,`訂製商品名稱`,`訂製商品型號`,`訂製商品數量`,`訂製商品單位`,`訂製商品金額`,`訂製工程報價單訂製工程報價單編號`,`供應商供應商代號`) VALUES ('NULL',:design_name,:design_type,:design_count,:design_countname,:design_dollar,:design_id,(select 供應商代號 from 供應商 where 供應商名稱=:design_supplier_name))";
                            $input=array(':design_name'=>$design_name[$i],':design_type'=>$design_type[$i],':design_count'=>$design_count[$i],':design_countname'=>$design_countname[$i],':design_dollar'=>$design_dollar[$i],':design_id'=>$design_id,':design_supplier_name'=>$design_supplier_name[$i]);
                            $db = Database::initDB();
                            $temp=$db-> prepare($SQL);
                            $check=$temp->execute($input);
                            $db=NULL;
                            if(!($check>0)){
                                $total_chack=false;
                            }
                        }
                    }
                }else{
                    $total_chack=false;
                }//update 進貨單,進貨紀錄  若任何一條輸入錯誤則$total_chack會變成false
                
                if($total_chack){
                    echo '修改成功';
                }else{
                    echo '修改失敗';
                }//修改區塊
            }else if($hidden=="design_del"){
                $total_chack=true;
                $SQL="DELETE FROM 訂製商品明細 WHERE 訂製工程報價單訂製工程報價單編號=:design_id";
                $db = Database::initDB();
                $temp=$db-> prepare($SQL);
                $check=$temp->execute(array(':design_id'=>$design_id));
                $db=NULL;
                if(!($check>0)){
                    $total_chack=false;
                }
                
                $SQL="DELETE FROM 訂製工程報價單 WHERE 訂製工程報價單編號=:design_id";
                $db = Database::initDB();
                $temp=$db-> prepare($SQL);
                $check=$temp->execute(array(':design_id'=>$design_id));
                $db=NULL;
                if(!($check>0)){
                    $total_chack=false;
                }
                
                if($total_chack){
                    echo '刪除成功';
                    $design_id="";
                }else{
                    echo '刪除失敗';
                }
            
            //刪除區塊
            }else if($hidden=="design_receipt"){
                $SQL="UPDATE 訂製工程報價單 SET `訂製進度` =  '訂製中' WHERE  `訂製工程報價單編號` =  :design_id;";
                $input=array(':design_id'=>$design_id);
                $db = Database::initDB();
                $temp=$db->prepare($SQL);
                $check=$temp->execute($input);
                $db=NULL;
                
                if($check>0){
                    echo '訂製中';
                }
                
            
            }else if($hidden=="design_end"){
                    $SQL="UPDATE 訂製工程報價單 SET `訂製進度` =  '出貨中' WHERE  `訂製工程報價單編號` =  :design_id;";
                    $input=array(':design_id'=>$design_id);
                    $db = Database::initDB();
                    $temp=$db->prepare($SQL);
                    $check=$temp->execute($input);
                    $db=NULL;
                    if($check>0){
                        echo "已出貨";
                    }else{
                    echo "尚未出貨";
                    }
            }
        ?>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;跳轉中請稍後~~~~
        <form action="design_search.php" method="POST" id="form_id">
            <input type="hidden" name="design_id" value=<?php echo $design_id; ?>>
        </form>
        <script type="text/javascript">
            setTimeout("form_id.submit();",1000);
        </script>
    </body>
</html>
