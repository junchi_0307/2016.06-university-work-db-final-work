<?php
    require 'mysql_connect.php';
    $SD_id=$_POST['SD_id'];
    $html2 = '<div id="text_zone"><table><tr><td>商品名稱</td><td>數量</td></tr>';
    $db = Database::initDB();
    $result = $db->query("select 商品名稱,銷貨數量 from 銷貨紀錄 join 商品 on 銷貨紀錄.商品商品型號=商品.商品型號 where 銷貨單銷售單號 = '" . $SD_id . "'");
    $db=NULL;
    $text_total=0;
    foreach($result->fetchAll() as $row){
        $html2 .= '<tr><td><input type="text" name="SD_list[' . $text_total . ']" value="' . $row['商品名稱'] . '" id="SD_list[' . $text_total . ']"></td>';
        $html2 .= '<td><input type="text" name="SD_list_Q[' . $text_total . ']" value="' . $row['銷貨數量'] . '" id="SD_list_Q[' . $text_total . ']"></td></tr>';
        $text_total++;
    }
    if($text_total==0){
        $html2 .= '<tr><td><input type="text" name="SD_list[' . $text_total . ']" value="" id="SD_list[' . $text_total . ']"></td>';
        $html2 .= '<td><input type="text" name="SD_list_Q[' . $text_total . ']" value="" id="SD_list_Q[' . $text_total . ']"></td></tr>';
        $text_total++;
    }
    $html2 .='</table></div>';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <script type="text/javascript">
            var text_total = <?php echo $text_total ?>;
            function add_text() {//增加一行
                var html = '<table><tr><td>商品名稱</td><td>數量</td></tr>';
                for (var i = 0; i < text_total; i++) {
                    var text_temp = document.getElementById('SD_list[' + i + ']').value;
                    var text_temp_Q = document.getElementById('SD_list_Q[' + i + ']').value;
                    html += '<tr><td><input type="text" name="SD_list[' + i + ']" value="' + text_temp + '" id="SD_list[' + i + ']"></td>';
                    html += '<td><input type="text" name="SD_list_Q[' + i + ']" value="' + text_temp_Q + '" id="SD_list_Q[' + i + ']"></td></tr>';
                }
                html += '<tr><td><input type="text" name="SD_list[' + text_total + ']" value="" id="SD_list[' + text_total + ']"></td>';
                html += '<td><input type="text" name="SD_list_Q[' + text_total + ']" value="" id="SD_list_Q[' + text_total + ']"></td></tr></table>';
                text_total++;
                document.getElementById('text_zone').innerHTML = html;
            }
            function clear_text() {//清除
                text_total--;
                var html = '<table><tr><td>商品名稱</td><td>數量</td></tr>';
                for (var i = 0; i <= text_total; i++) {
                    html += '<tr><td><input type="text" name="SD_list[' + i + ']" value="" id="SD_list[' + i + ']"></td>';
                    html += '<td><input type="text" name="SD_list_Q[' + i + ']" value="" id="SD_list_Q[' + i + ']"></td></tr>';
                }
                document.getElementById('text_zone').innerHTML = html;
                text_total++;
            }
            function del_text() {//減少一行
                if (text_total > 1) {
                    text_total--;
                    var html = '<table><tr><td>商品名稱</td><td>數量</td></tr>';
                    for (var i = 0; i < text_total; i++) {
                        var text_temp = document.getElementById('SD_list[' + i + ']').value;
                        var text_temp_Q = document.getElementById('SD_list_Q[' + i + ']').value;
                        html += '<tr><td><input type="text" name="SD_list[' + i + ']" value="' + text_temp + '" id="SD_list[' + i + ']"></td>';
                        html += '<td><input type="text" name="SD_list_Q[' + i + ']" value="' + text_temp_Q + '" id="SD_list_Q[' + i + ']"></td></tr>';
                    }
                    document.getElementById('text_zone').innerHTML = html;
                }
            }
            function acc() {//確認有無所有欄位皆有值
                for (var i = 0; i < text_total; i++) {
                    if (document.getElementById('SD_list[' + i + ']').value == "") {
                        document.getElementById('display').innerHTML = '<font style="color:red;"  size="4"><<請勿留下空白欄位>></font>';
                        return false;
                    }
                    if (document.getElementById('SD_list_Q[' + i + ']').value == "") {
                        document.getElementById('display').innerHTML = '<font style="color:red;"  size="4"><<請勿留下空白欄位>></font>';
                        return false;
                    }
                }
                if ((document.getElementById("SD_id").value == "") || (document.getElementById("employee_name").value == "") || (document.getElementById("delivery_date").value == "") || (document.getElementById("SD_date").value == "") || (document.getElementById("customer_name").value == "")) {
                    document.getElementById('display').innerHTML = '<font style="color:red;"  size="4"><<請勿留下空白欄位>></font>';
                    return false;
                }
                return true;
            }
        </script>
    </head>

    <body>
        <div id='display'>
        </div>
        <form method="POST" action="SD_temp.php" onsubmit="return acc();">
            <?php
                $db = Database::initDB();
                $result = $db->query("select 銷售單號,員工姓名,送貨日期,銷貨日期,顧客姓名,銷貨進度,備註 from 銷貨單 join 員工 on 銷貨單.員工員工ID=員工.員工ID join 顧客 on 銷貨單.顧客顧客ID=顧客.顧客ID where 銷售單號='" . $SD_id . "'");
                $db=NULL;
                $row=$result->fetch();
            ?>
            </br></br></br>
            <table>
                <tr>
                    <td>銷售單號：</td><td><input type="text" name="SD_id" value="<?php echo $row['銷售單號']?>" id="SD_id" readonly></td>
                    <td>員工姓名：</td><td><input type="text" name="employee_name" value="<?php echo $row['員工姓名']?>" id="employee_name" readonly></td>
                </tr>
                <tr>
                    <td>顧客：</td><td><input type="text" name="customer_name" value="<?php echo $row['顧客姓名']?>" id="customer_name" readonly></td>
                </tr>
                <tr>
                    <td>進度：</td><td><input type="text" name="SD_speed" value="<?php echo $row['銷貨進度']?>" id="SD_speed" readonly></td>
                </tr>
                <tr>
                    <td>送貨日期：</td><td><input type="date" name="delivery_date" value="<?php echo $row['送貨日期']?>" id="delivery_date"></td>
                    <td>銷貨日期：</td><td><input type="date" name="SD_date" value="<?php echo $row['銷貨日期']?>" id="SD_date"></td>
                </tr>
                <tr>
                    <td><input type="button" value="新增一行" onclick="add_text()"></td><td><input type="button" value="刪除一行" onclick="del_text()"></td>
                </tr>
            </table>
            <div id="text_zone">
                <?php echo $html2?>
            </div>
            <textarea name="remark" row="10" cols="20" wrap="off"><?php echo $row['備註']?></textarea></br>
            <input type="hidden" name="hidden" value="SD_revise">
            <input type="submit" value="修改完成" onclick="acc()">
            <input type="button" value="清除" onclick="clear_text()">
        </form>
    </body>
</html>