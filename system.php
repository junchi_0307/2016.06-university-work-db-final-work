<?php
require 'mysql_connect.php';
$func='connect';
if(!empty($_POST['func'])){
    $func=$_POST['func'];
}
switch ($func) {
    default:
    case 'update':
        require 'update.php';
        break;
    case 'register':
        require 'register.php';
        break;
    case 'register_finish':
        require 'register_finish.php';
        break;
    case 'delete':
        require 'delete.php';
        break;
    case 'logout':
        require 'logout.php';
        break;
    case 'update_finish':
        require 'update_finish.php';
        break;
    case 'connect':
        require 'connect.php';
        break;
    case 'register_customer':
        require 'register_customer.php';
        break;
    case 'update_customer':
        require 'update_customer.php';
        break;
    case 'delete_customer':
        require 'delete_customer.php';
        break;
    case 'register_supplier':
        require 'register_supplier.php';
        break;
    case 'update_supplier':
        require 'update_supplier.php';
        break;
    case 'delete_supplier':
        require 'delete_supplier.php';
        break;
    case 'register_warehouse':
        require 'register_warehouse.php';
        break;
    case 'update_warehouse':
        require 'update_warehouse.php';
        break;
    case 'delete_warehouse':
        require 'delete_warehouse.php';
        break;
            case 'register_goodsspe':
        require 'register_goodsspe.php';
        break;
    case 'update_goodsspe':
        require 'update_goodsspe.php';
        break;
    case 'delete_goodsspe':
        require 'delete_goodsspe.php';
        break;
    case 'register_goodsspe_finish':
        require 'register_goodsspe_finish.php';
        break;
    case 'update_goodsspe_finish':
        require 'update_goodsspe_finish.php';
        break;
    case 'detail_goods':
        require 'goods.php';
        break;
    case 'register_goods':
        require 'register_goods.php';
        break;
    case 'update_goods':
        require 'update_goods.php';
        break;
    case 'update_goods_finish':
        require 'update_goods_finish.php';
        break;
    case 'delete_goods':
        require 'delete_goods.php';
        break;
}
?>

