<html>
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <?php
        require 'mysql_connect.php';
        $hidden=$_POST['hidden'];
        $MM_id="";
            if($hidden=="MM_new"){
                $total_chack=true;
                $MM_id=$_POST['MM_id'];
                $MM_list=$_POST['MM_list'];
                $MM_list_P=$_POST['MM_list_P'];
                $MM_list_Q=$_POST['MM_list_Q'];
                $supplier_name=$_POST['supplier_name'];
                $employee_name=$_POST['employee_name'];
                $message_date=$_POST['message_date'];
                $MM_date=$_POST['MM_date'];
                $MM_speed=$_POST['MM_speed'];
                $remark=$_POST['remark'];
                
                {$supplier_name_check=false;
                $db = Database::initDB();
                $temp = $db -> query("select 供應商名稱 from 供應商");
                $db=NULL;
                foreach($temp->fetchall() as $row){
                    if($row[0]==$supplier_name){
                        $supplier_name_check=true;
                    }
                }
                
                
                $employee_name_check=false;
                $db = Database::initDB();
                $temp = $db -> query("select 員工姓名 from 員工");
                $db=NULL;
                foreach($temp->fetchall() as $row){
                    if($row[0]==$employee_name){
                        $employee_name_check=true;
                    }
                }
                
                $MM_list_check_total=true;
                for($i=0;$i<count($MM_list);$i++){
                    $MM_list_check=false;
                    $db = Database::initDB();
                    $temp = $db -> query("select 商品名稱 from 商品");
                    $db=NULL;
                    foreach($temp->fetchall() as $row){
                        if($row[0]==$MM_list[$i]){
                            $MM_list_check=true;
                        }
                    }
                    if(!$MM_list_check){
                        $MM_list_check_total=false;
                    }
                }}//測試是否有輸入的商品名稱,供應商名稱,員工姓名
                
                
                if($MM_list_check_total && $employee_name_check && $supplier_name_check){

                    $SQL="INSERT INTO 進貨單 (`進貨單號`,`通知日期`,`進貨日期`,`員工員工ID`,`供應商供應商代號`,`進貨進度`,`備註`) VALUES (:MM_id,:message_date,:MM_date,(select 員工ID from 員工 where 員工姓名=:employee_name), (select 供應商代號 from 供應商 where 供應商名稱=:supplier_name),:MM_speed,:remark)";
                    $input=array(':MM_id'=>$MM_id,':message_date'=>$message_date,':MM_date'=>$MM_date,':employee_name'=>$employee_name,':supplier_name'=>$supplier_name,':MM_speed'=>'未收貨',':remark'=>$remark);
                    $db = Database::initDB();
                    $temp=$db-> prepare($SQL);
                    $check=$temp->execute($input);
                    $db=NULL;
                    if(!($check>0)){
                        $total_chack=false;
                    }
                    if($total_chack){
                        for($i=0;$i<count($MM_list);$i++){
                            $SQL="INSERT INTO 進貨紀錄 (`進貨紀錄ID`,`進貨單進貨單號`,`商品商品型號`,`進貨單價`,`進貨數量`) VALUES (:MM_list_id,:MM_id,(select 商品型號 from 商品 where 商品名稱 = :MM_list),:MM_list_P,:MM_list_Q)";
                            $input=array(':MM_list_id'=>NULL,':MM_id'=>$MM_id,':MM_list'=>$MM_list[$i],':MM_list_P'=>$MM_list_P[$i],':MM_list_Q'=>$MM_list_Q[$i]);
                            $db = Database::initDB();
                            $temp=$db-> prepare($SQL);
                            $check=$temp->execute($input);
                            $db=NULL;
                            if(!($check>0)){
                                $total_chack=false;
                            }
                        }
                    }
                }else{
                    $total_chack=false;
                }//insert 進貨單,進貨紀錄  若任何一條輸入錯誤則$total_chack會變成false
                    
                if($total_chack){
                    echo '新增成功';
                }else{
                    echo '新增失敗';
                    $MM_id="";
                }//新增區塊
            }else if($hidden=="MM_revise"){
                $total_chack=true;
                $MM_id=$_POST['MM_id'];
                $MM_list=$_POST['MM_list'];
                $MM_list_P=$_POST['MM_list_P'];
                $MM_list_Q=$_POST['MM_list_Q'];
                $supplier_name=$_POST['supplier_name'];
                $employee_name=$_POST['employee_name'];
                $message_date=$_POST['message_date'];
                $MM_date=$_POST['MM_date'];
                $MM_speed=$_POST['MM_speed'];
                $remark=$_POST['remark'];
                
                {$supplier_name_check=false;
                $db = Database::initDB();
                $temp = $db -> query("select 供應商名稱 from 供應商");
                $db=NULL;
                foreach($temp->fetchall() as $row){
                    if($row[0]==$supplier_name){
                        $supplier_name_check=true;
                    }
                }
                $employee_name_check=false;
                $db = Database::initDB();
                $temp = $db -> query("select 員工姓名 from 員工");
                $db=NULL;
                foreach($temp->fetchall() as $row){
                    if($row[0]==$employee_name){
                        $employee_name_check=true;
                    }
                }
                
                $MM_list_check_total=true;
                for($i=0;$i<count($MM_list);$i++){
                    $MM_list_check=false;
                    $db = Database::initDB();
                    $temp = $db -> query("select 商品名稱 from 商品");
                    $db=NULL;
                    foreach($temp->fetchall() as $row){
                        if($row[0]==$MM_list[$i]){
                            $MM_list_check=true;
                        }
                    }
                    if(!$MM_list_check){
                        $MM_list_check_total=false;
                    }
                }
                    
                }//測試是否有輸入的商品名稱,供應商名稱,員工姓名
                
                if($MM_list_check_total && $employee_name_check && $supplier_name_check){
                    $SQL="DELETE FROM 進貨紀錄 WHERE 進貨單進貨單號 = :MM_id";
                    $db = Database::initDB();
                    $temp=$db-> prepare($SQL);
                    $check=$temp->execute(array(':MM_id'=>$MM_id));
                    $db=NULL;
                    /*if(!($check>0)){
                        $total_chack=false;
                    }*/
                    
                    $SQL="UPDATE 進貨單 SET `通知日期`=:message_date,`進貨日期`=:MM_date,`員工員工ID`=(select 員工ID from 員工 where 員工姓名=:employee_name), `供應商供應商代號`=(select 供應商代號 from 供應商 where 供應商名稱=:supplier_name),`進貨進度`=:MM_speed,`備註`=:remark where `進貨單號`=:MM_id";
                    $input=array(':MM_id'=>$MM_id,':message_date'=>$message_date,':MM_date'=>$MM_date,':employee_name'=>$employee_name,':supplier_name'=>$supplier_name,':MM_speed'=>'未收貨',':remark'=>$remark);
                    $db = Database::initDB();
                    $temp=$db-> prepare($SQL);
                    $check=$temp->execute($input);
                    $db=NULL;
                    if(!($check>0)){
                        $total_chack=false;
                    }
                    if($total_chack){
                        for($i=0;$i<count($MM_list);$i++){
                            $SQL="INSERT INTO 進貨紀錄 (`進貨紀錄ID`,`進貨單進貨單號`,`商品商品型號`,`進貨單價`,`進貨數量`) VALUES (:MM_list_id,:MM_id,(select 商品型號 from 商品 where 商品名稱 = :MM_list),:MM_list_P,:MM_list_Q)";
                            $input=array(':MM_list_id'=>$MM_list,':MM_id'=>$MM_id,':MM_list'=>$MM_list[$i],':MM_list_P'=>$MM_list_P[$i],':MM_list_Q'=>$MM_list_Q[$i]);
                            $db = Database::initDB();
                            $temp=$db-> prepare($SQL);
                            $check=$temp->execute($input);
                            $db=NULL;
                            if(!($check>0)){
                                $total_chack=false;
                            }
                        }
                    }
                }else{
                    $total_chack=false;
                }//update 進貨單,進貨紀錄  若任何一條輸入錯誤則$total_chack會變成false
                
                if($total_chack){
                    echo '修改成功';
                }else{
                    echo '修改失敗';
                }//修改區塊
            }else if($hidden=="MM_del"){
                $total_chack=true;
                $MM_id=$_POST['MM_id'];
                $SQL="DELETE FROM 進貨紀錄 WHERE 進貨單進貨單號 = :MM_id";
                $db = Database::initDB();
                $temp=$db-> prepare($SQL);
                $check=$temp->execute(array(':MM_id'=>$MM_id));
                $db=NULL;
                if(!($check>0)){
                    $total_chack=false;
                }
                
                $SQL="DELETE FROM 進貨單 WHERE 進貨單號 = :MM_id";
                $db = Database::initDB();
                $temp=$db-> prepare($SQL);
                $check=$temp->execute(array(':MM_id'=>$MM_id));
                $db=NULL;
                if(!($check>0)){
                    $total_chack=false;
                }
                
                if($total_chack){
                    echo '刪除成功';
                    $MM_id="";
                }else{
                    echo '刪除失敗';
                }
            
            //刪除區塊
            }else if($hidden=="MM_receipt"){
                $MM_id=$_POST['MM_id'];
                $SQL="UPDATE 進貨單 SET `進貨進度` =  '已收貨' WHERE  `進貨單號` =  :MM_id;";
                $input=array(':MM_id'=>$MM_id);
                $db = Database::initDB();
                $temp=$db->prepare($SQL);
                $check=$temp->execute($input);
                $db=NULL;
                
                if($check>0){
                    echo '收貨完成';
                }
                
            }
        ?>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;跳轉中請稍後~~~~
        <form action="MM_search.php" method="POST" id="form_id">
            <input type="hidden" name="MM_id" value=<?php echo $MM_id; ?>>
        </form>
        <script type="text/javascript">
            setTimeout("form_id.submit();",1000);
        </script>
    </body>
</html>