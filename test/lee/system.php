<?php
require 'mysql_connect.php';
$func='connect';
if(!empty($_POST['func'])){
    $func=$_POST['func'];
}
switch ($func) {
    default:
    case 'update':
        require 'update.php';
        break;
    case 'register':
        require 'register.php';
        break;
    case 'register_finish':
        require 'register_finish.php';
        break;
    case 'delete':
        require 'delete.php';
        break;
    case 'logout':
        require 'logout.php';
        break;
    case 'update_finish':
        require 'update_finish.php';
        break;
    case 'connect':
        require 'connect.php';
        break;
}
?>

