<html>
    <head>
        <meta charset="UTF-8">
        <script type="text/javascript">
            var text_total = 1;
            function add_text() {//增加一行
                var html = '<table><tr><td>商品名稱</td><td>數量</td><td>進貨單價</td></tr>';
                for (var i = 0; i < text_total; i++) {
                    var text_temp = document.getElementById('MM_list[' + i + ']').value;
                    var text_temp_Q = document.getElementById('MM_list_Q[' + i + ']').value;
                    var text_temp_P = document.getElementById('MM_list_P[' + i + ']').value;
                    html += '<tr><td><input type="text" name="MM_list[' + i + ']" value="' + text_temp + '" id="MM_list[' + i + ']"></td>';
                    html += '<td><input type="text" name="MM_list_Q[' + i + ']" value="' + text_temp_Q + '" id="MM_list_Q[' + i + ']"></td>';
                    html += '<td><input type="text" name="MM_list_P[' + i + ']" value="' + text_temp_P + '" id="MM_list_P[' + i + ']"></td></tr>';
                }
                html += '<tr><td><input type="text" name="MM_list[' + text_total + ']" value="" id="MM_list[' + text_total + ']"></td>';
                html += '<td><input type="text" name="MM_list_Q[' + text_total + ']" value="" id="MM_list_Q[' + text_total + ']"></td>';
                html += '<td><input type="text" name="MM_list_P[' + text_total + ']" value="" id="MM_list_P[' + text_total + ']"></td></tr></table>';
                text_total++;
                document.getElementById('text_zone').innerHTML = html;
            }
            function clear_text() {//清除
                text_total--;
                var html = '<table><tr><td>商品名稱</td><td>數量</td><td>進貨單價</td></tr>';
                for (var i = 0; i <= text_total; i++) {
                    html += '<tr><td><input type="text" name="MM_list[' + i + ']" value="" id="MM_list[' + i + ']"></td>';
                    html += '<td><input type="text" name="MM_list_Q[' + i + ']" value="" id="MM_list_Q[' + i + ']"></td>';
                    html += '<td><input type="text" name="MM_list_P[' + i + ']" value="" id="MM_list_P[' + i + ']"></td></tr>';
                }
                document.getElementById('text_zone').innerHTML = html;
                text_total++;
            }
            function del_text() {//減少一行
                if (text_total > 1) {
                    text_total--;
                    var html = '<table><tr><td>商品名稱</td><td>數量</td><td>進貨單價</td></tr>';
                    for (var i = 0; i < text_total; i++) {
                        var text_temp = document.getElementById('MM_list[' + i + ']').value;
                        var text_temp_Q = document.getElementById('MM_list_Q[' + i + ']').value;
                        var text_temp_P = document.getElementById('MM_list_P[' + i + ']').value;
                        html += '<tr><td><input type="text" name="MM_list[' + i + ']" value="' + text_temp + '" id="MM_list[' + i + ']"></td>';
                        html += '<td><input type="text" name="MM_list_Q[' + i + ']" value="' + text_temp_Q + '" id="MM_list_Q[' + i + ']"></td>';
                        html += '<td><input type="text" name="MM_list_P[' + i + ']" value="' + text_temp_P + '" id="MM_list_P[' + i + ']"></td></tr>';
                    }
                    document.getElementById('text_zone').innerHTML = html;
                }
            }
            function acc() {//確認有無所有欄位皆有值
                for (var i = 0; i < text_total; i++) {
                    if (document.getElementById('MM_list[' + i + ']').value == "") {
                        document.getElementById('display').innerHTML = '<font style="color:red;"  size="4"><<請勿留下空白欄位>></font>';
                        return false;
                    }
                    if (document.getElementById('MM_list_P[' + i + ']').value == "") {
                        document.getElementById('display').innerHTML = '<font style="color:red;"  size="4"><<請勿留下空白欄位>></font>';
                        return false;
                    }
                    if (document.getElementById('MM_list_Q[' + i + ']').value == "") {
                        document.getElementById('display').innerHTML = '<font style="color:red;"  size="4"><<請勿留下空白欄位>></font>';
                        return false;
                    }
                }
                if ((document.getElementById("MM_id").value == "") || (document.getElementById("employee_name").value == "") || (document.getElementById("message_date").value == "") || (document.getElementById("MM_date").value == "") || (document.getElementById("supplier_name").value == "")) {
                    document.getElementById('display').innerHTML = '<font style="color:red;"  size="4"><<請勿留下空白欄位>></font>';
                    return false;
                }
                return true;
            }
        </script>
    </head>

    <body>
        
        <div id="display"><!--若有空白欄位，則這邊會更新顯示-->
            
        </div>
        <form action="MM_temp.php" method="POST" onsubmit="return acc();">
            進貨單號：<input type="text" name="MM_id" value="" id="MM_id">
            員工姓名：<input type="text" name="employee_name" value="" id="employee_name"></br>
            通知日期：<input type="date" name=" message_date" value="" id="message_date">
            進貨日期：<input type="date" name="MM_date" value="" id="MM_date"></br>
            供應商：<input type="text" name="supplier_name" value="" id="supplier_name"></br>
            <input type="button" value="新增一行" onclick="add_text()">
            <input type="button" value="刪除一行" onclick="del_text()"></br>
            <div id="text_zone">
                <table>
                    <tr>
                        <td>
                            商品名稱
                        </td>
                        <td>
                            數量
                        </td>
                        <td>
                            進貨單價
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" name="MM_list[0]" id="MM_list[0]">
                        </td>
                        <td>
                            <input type="text" name="MM_list_Q[0]" id="MM_list_Q[0]">
                        </td>
                        <td>
                            <input type="text" name="MM_list_P[0]" id="MM_list_P[0]">
                        </td>
                    </tr>
                </table>
            </div>
            <textarea name="remark" row="10" cols="20" warp="off"></textarea></br>
            <input type="submit" value="送出" onclick="acc()">
            <input type="button" value="清除" onclick="clear_text()">
            <input type="hidden" name="hidden" value="MM_new">
        </form>
    </body>
</html>
